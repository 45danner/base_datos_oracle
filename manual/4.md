# 4. Recuperación de algunos campos (select)

Hemos aprendido cómo ver todos los registros de una tabla:

```sql
select *from libros;
```

El comando "select" recupera los registros de una tabla. Con el asterisco (*) indicamos que seleccione todos los campos de la tabla que nombramos.

Podemos especificar el nombre de los campos que queremos ver separándolos por comas:

```sql
select titulo,autor,editorial from libros;
```

En la sentencia anterior la consulta mostrará sólo los campos "titulo", "autor" y "editorial". En la siguiente sentencia, veremos los campos correspondientes al título y precio de todos los libros:

```sql
select titulo, precio from libros;
```

Para ver solamente la editorial y la cantidad de libros tipeamos:

```sql
select editorial,cantidad from libros;
```

Servidor de MySQL instalado en forma local.
Ingresemos al programa "Workbench" y procedamos a crear una tabla, insertar algunas filas y mostrar solo algunas columnas de dicha tabla:

```sql
drop table if exists libros;

create table libros(
    titulo varchar(100),
    autor varchar(30),
    editorial varchar(15),
    precio float,
    cantidad integer
);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',45.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta',25,200);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Planeta',15.8,200);

select titulo,precio from libros;

select editorial,cantidad from libros;

select * from libros;
```

# Ejercicios laboratorio

Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.

Eliminamos la tabla, si existe:

```sql
drop table if exists libros;
Creamos la tabla "libros":

create table libros(
  titulo varchar(20),
  autor varchar(30),
  editorial varchar(15),
  precio float,
  cantidad integer
);
 ```

Veamos la estructura de la tabla:

```sql
describe libros;
```

Ingresamos algunos registros:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',45.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta',25,200);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Planeta',15.8,200);
```

Para ver todos los campos de una tabla tipeamos:

```sql
select *from libros;
```

Con el asterisco (*) indicamos que seleccione todos los campos de la tabla.

Para ver solamente el título, autor y editorial de todos los libros especificamos los nombres de los campos separados por comas:

```sql
select titulo,autor,editorial from libros;
```

La siguiente sentencia nos mostrará los títulos y precios de todos los libros:

```sql
select titulo,precio from libros;
```

Para ver solamente la editorial y la cantidad de libros tipeamos:

```sql
select editorial,cantidad from libros;
```

# Ejercicios propuestos

## Ejercicio 01

Un videoclub que alquila películas en video almacena la información de sus películas en alquiler en una tabla llamada "peliculas".

1. Elimine la tabla, si existe:

```sql
drop table if exists peliculas;
```

2. Cree la tabla:

```sql
create table peliculas(
  titulo varchar(20),
  actor varchar(20),
  duracion integer,
  cantidad integer
);
```

3. Vea la estructura de la tabla:

```sql
describe peliculas;
```

4. Ingrese los siguientes registros:

```sql
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,2);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',90,2);
```

5. Realice un "select" mostrando solamente el título y actor de todas las películas:

```sql
select titulo,actor from peliculas;
```
6. Muestre el título y duración de todas las peliculas.

7. Muestre el título y la cantidad de copias.

## Ejercicio 02

A. Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

1. Elimine la tabla, si existe:

```sql
 drop table if exists empleados;
```

2. Cree la tabla:

```sql
 create table empleados(
  nombre varchar(20),
  documento varchar(8), 
  sexo varchar(1),
  domicilio varchar(30),
  sueldobasico float
 );
```

3. Vea la estructura de la tabla:

```sql
describe empleados;
```

4. Ingrese algunos registros:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22345678','m','Sarmiento 123',300);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24345678','f','Colon 134',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Marcos Torres','27345678','m','Urquiza 479',800);
```

5. Muestre todos los datos de los empleados.

6. Muestre el nombre y documento de los empleados.

7. Realice un "select" mostrando el nombre, documento y sueldo básico de todos los empleados.


B) Un comercio que vende artículos de computación registra la información de sus 
productos en la tabla llamada "articulos".

1. Elimine la tabla si existe:

 drop table if exists articulos;

2. Cree la tabla "articulos" con los campos necesarios para almacenar los siguientes datos:

* código del artículo: entero,
* nombre del artículo: 20 caracteres de longitud,
* descripción: 30 caracteres de longitud,
* precio: float.

3. Vea la estructura de la tabla (describe).

4. Ingrese algunos registros:

```sql
 insert into articulos (codigo, nombre, descripcion, precio)
  values (1,'impresora','Epson Stylus C45',400.80);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (2,'impresora','Epson Stylus C85',500);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (3,'monitor','Samsung 14',800);
```

5. Muestre todos los campos de todos los registros.

6. Muestre sólo el nombre, descripción y precio.